﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using SecuStore.Core.Domain;

namespace SecuStore.Core
{
    public class SecureTools
    {
        public Envelope Encrypt(string password, string clearText)
        {
            return new Envelope();
        }

        public IBuffer ComputeHash(string value)
        {
            IBuffer buffUtf8Msg = CryptographicBuffer.ConvertStringToBinary(value, BinaryStringEncoding.Utf8);
            var objAlgProv = HashAlgorithmProvider.OpenAlgorithm("SHA256");
            var strAlgNameUsed = objAlgProv.AlgorithmName;

            var buffHash = objAlgProv.HashData(buffUtf8Msg);

            if (buffHash.Length != objAlgProv.HashLength)
            {
                throw new Exception("There was an error creating the hash");
            }

            return buffHash;

            //// Convert the hash to a string (for display).
            //String strHashBase64 = CryptographicBuffer.EncodeToBase64String(buffHash);

            //// Return the encoded string
            //return strHashBase64;
        }

        
    }
}
