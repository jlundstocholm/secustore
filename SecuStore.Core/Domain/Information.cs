﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuStore.Core.Domain
{
    public class Information
    {
        public Guid UserId { get; set; }
        public Envelope Cipher { get; set; }
    }
}
