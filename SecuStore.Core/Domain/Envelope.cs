﻿namespace SecuStore.Core.Domain
{
    public class Envelope
    {
        public string InitializationVector { get; set; }
        public string CipherText { get; set; }
        public int HashIterationCount { get; set; }
    }
}